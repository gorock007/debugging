package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoSettings;

import library.entities.ILoan.LoanState;
import library.entities.helpers.BookHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class LibraryTest {
	ILoan loan;
	@Mock IBook mockBook;
	@Mock IPatron mockPatron;
	@Mock IBookHelper mockBookHelper;
	@Mock IPatronHelper mockPatronHelper;
	@Mock ILoanHelper mockLoanHelper;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		java.util.Date dueDate = Calendar.getInstance().getDueDate(0);
		loan = new Loan(mockBook, mockPatron, dueDate, LoanState.OVER_DUE, 1);
	}
	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void testCalculateOverDueFineBug2() {
		//arrange
		Library library = new Library(mockBookHelper, mockPatronHelper, mockLoanHelper);
		Calendar.getInstance().incrementDate(4);
		double expected = 4;
		
		//act
		double actual = library.calculateOverDueFine(loan);
		
		//equals
		assertEquals(expected,actual);
	}
	
	@Test
	void testCalculateOverDueFineBug1() {
		//arrange
		Library library = new Library(mockBookHelper, mockPatronHelper, mockLoanHelper);
		Calendar.getInstance().incrementDate(1);
		double expected = 1;
		
		//act
		double actual = library.calculateOverDueFine(loan);
		
		//equals
		assertEquals(expected,actual);
	}
	
	
}
